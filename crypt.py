"""
    The "crypt" module
    ======================

    A utiliser pour crypter / décrypter des strings
"""

DECALAGE = 13

A = 65
Z = 90
a = 97
z = 122


def compareLettre(indexLettre, bInf, bSup):
    """
    Compare la lettre.

    :param indexLettre: code Ascii de la lettre à traiter
    :param bInf: borne inférieur
    :param bSup: borne supérieur
    :return: code Ascii de la lettre traité
    """
    if indexLettre < bInf:
        indexLettre += bSup - bInf + 1
    if indexLettre > bSup:
        indexLettre += -bSup + bInf - 1
    return indexLettre


def decalCircu(lettre, decal):
    """
    Retourne le caractère avec un décalage circulaire.

    :param lettre: lettre à traiter
    :param decal: valeur du décalage.
    :return: lettre traitée
    :rtype: string
    """
    indexLettre = ord(lettre)
    if indexLettre >= A and indexLettre <= Z:
        indexLettre = compareLettre(indexLettre+decal, A, Z)
    elif indexLettre >= a and indexLettre <= z:
        indexLettre = compareLettre(indexLettre+decal, a, z)
    else:
        return lettre
    return chr(indexLettre)


def decaler(lettre, decal, circulaire):
    """
    Retourne le caractère avec un décalage.

    :param lettre: Lettre à traiter
    :param decal: valeur du décalage
    :param circulaire: 1 si cryptage circulaire sinon 0
    :return: lettre décalée
    :rtype: string
    """
    if circulaire == 1:
        return decalCircu(lettre, decal)
    elif circulaire == 0:
        return chr(ord(lettre) + decal)


def cryptDecrypt(oldPhrase, decal, circulaire):
    """
    Retourne la nouvelle chaine avec un décalage.

    :param oldPhrase: Phrase à traiter
    :param decal: valeur du décalage
    :param circulaire: 1 si cryptage circulaire sinon 0
    :return: Phrase traitée
    :rtype: string
    """
    return ''.join([decaler(lettre, decal, circulaire) for lettre in oldPhrase])


def crypt(phrase, circulaire=0, decal=DECALAGE):
    """
    Fonction qui retourne la chaine cryptée

    :param phrase: phrase à crypter
    :param circulaire: 1 si cryptage circulaire sinon 0.
    :param decal: valeur du décalage
    :return: phrase cryptée
    :rtype: string

    :Example:

    >>> crypt("abc",0,8)
    nop
    >>> crypt("ijk",0,8)
    qrs
    """
    return cryptDecrypt(phrase, decal, circulaire)


def decrypt(phraseCrypted, circulaire=0, decal=DECALAGE):
    """
    Fonction qui retourne la chaine décryptée

    :param phraseCrypted: phrase à décrypter
    :param circulaire: 1 si cryptage circulaire sinon 0.
    :param decal: valeur du décalage
    :return: phrase decryptée
    :rtype: string

    :Example:

    >>> decrypt("nop",0,13)
    abc
    >>> decrypt("ijk",0,8)
    abc
    """
    return cryptDecrypt(phraseCrypted, -decal, circulaire)
