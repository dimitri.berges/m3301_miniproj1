# ![](imgs/logo.png)

## Cryptage de Cesar

Mini-Projet de la matière "Méthodologie de la production d'application" M3301, 
en langage Python avec le chiffre de César qui consiste à décaler les lettres 
de l'alphabet d'un certain rang.

## Auteurs

- [Hugo Gennevée](https://gitlab.com/HugoGennevee)
- [Dimitri Bergès](https://gitlab.com/Dixmis)

## Lancer le projet

```bash
$ python3 -m interface.py
```

## Tester le projet

```bash
$ python3 -m unittest
```
